options compress=yes threads;

%let basePath = D:\PROGRAMOWANIE\SAS\PROJEKT_SGH\;

%let inDataPath = "&basePath.DATA\IN";
%let outDataPath = "&basePath.DATA\OUT_V";

libname inLib &inDataPath access=readonly;
libname outLib &outDataPath;

%let inData = inLib.abt_sam_beh_valid;
%let targetFunction = default_cus12;

%let missingsThreshold = 0.3;
%let outliersThreshlod = 0.9;

/*converts char variable to numeric*/
%macro ChangeVariableToNumeric(charVariableName, numericVariableName);
	proc sql noprint;	
		select
			distinct(&charVariableName) AS VALUES
		into
			:char_values
		separated by '#'
		from &inData.;
	quit;

	%let count = &sqlobs;

	%do i = 1 %to (&count);
		%let char_value = %scan(&char_values, &i, '#');

		data outLib.prepared_data;
			set outLib.prepared_data;

			if &charVariableName="&char_value" then &numericVariableName = &i;
		run;
	
		data outLib.char_numeric_conversion_legend;
			set outLib.char_numeric_conversion_legend end=eof;
			output;
			if eof then do;
				variable_name = "&charVariableName";
				char_value = "&char_value";
				numeric_value = %eval(&i - 1);
				output;
			end;
		run;
	%end;
%mend;

/*invokes data preparation chain */
%macro DataPreparation;
	data outLib.char_numeric_conversion_legend;
		length variable_name $50;
		length char_value $50;
		length numeric_value 3;
	run;

	data outLib.prepared_data;
		set &inData.;
		length app_num_job_code 3;
		length app_num_marital_status 3;
		length app_num_city 3;
		length app_num_home_status 3;
		length app_num_cars 3;
	run;
 
	%ChangeVariableToNumeric(app_char_job_code, app_num_job_code);
	%ChangeVariableToNumeric(app_char_marital_status, app_num_marital_status);
	%ChangeVariableToNumeric(app_char_city, app_num_city);
	%ChangeVariableToNumeric(app_char_home_status, app_num_home_status);
	%ChangeVariableToNumeric(app_char_cars, app_num_cars);	

	data outLib.data_app;
		set &inData.;
		keep period &targetFunction app:;
	run;

	data outLib.data_act;
		set &inData.;
		keep period &targetFunction act:;
	run;

	data outLib.data_ags;
		set &inData.;
		keep period &targetFunction ags:;
	run;

	data outLib.data_agr;
		set &inData.;
		keep period &targetFunction agr:;
	run;
%mend;

/* <Ex.1> */
/* drops variables that exceeds missing percent threshold */
%macro FilterVarsMissingThreshold(inDataSet, outDataSet, threshold);
	ods output summary=summary_stats;
	proc means data=&inDataSet. stackods n nmiss nway;		
	run;

	data outLib.percent_missing;
		set summary_stats;
		percent_missing = nmiss/sum(n, nmiss);
	run;

	data outLib.percent_missing;
		set outLib.percent_missing;

		where percent_missing < &threshold;
	run;

	proc sort data=outLib.percent_missing;
		by descending percent_missing;
	run;

	proc sql;
		SELECT 
			Variable
		INTO
			:variables_percent
		SEPARATED BY
			' '	
		FROM
			outLib.percent_missing;	
	quit;

	data &outDataSet.;
		set &inDataSet.;
		keep &variables_percent &targetFunction period;
	run;
	ods output close;
%mend;

/* counts missing perced for selected period */
%macro CountMissingsForPeriod(period, inDataSet, outDataSet);
	data data_&period;
		set &inDataSet.;
		where period like "&period%";
	run;

	ods output summary=summary_stats;
	proc means data=data_&period stackods n nmiss nway;		
	run;	

	data stats_percent_&period;
		set summary_stats;
		missing_data_percentage = (nmiss/sum(n, nmiss))*100;
	run;

	proc summary data=stats_percent_&period nway noprint;
		var missing_data_percentage;	
		output out=avg (drop=_:) mean=;
	run;

	proc sql noprint;
		SELECT
			missing_data_percentage
		INTO
			:percentage
		FROM 
			avg;
	run;

	proc sql noprint;
		INSERT INTO &outDataSet.
		VALUES 
		(
			"&period", 
			&percentage
		);
	run;

	proc delete data=data_&period;
	run;

	proc delete data=summary_stats;
	run;

	proc delete data=stats_percent_&period;
	run;

	proc delete data=avg;
	run;

	ods output close;
%mend;

/*counts missing percents for all periods in input dataset */
%macro CountMissingsForAllPeriods(inDataSet, outDataSet);
	proc sql;
		CREATE TABLE &outDataSet. 
		(
			period VARCHAR NOT NULL,
			missing_data_percentage INT NOT NULL
		);
	run;
	
	proc sql noprint;
		SELECT DISTINCT
			period
		INTO
			:periods
		SEPARATED BY
			'#'
		FROM
			&inDataSet.;
	run;

	%let periodsCount = &sqlObs;

	%do i = 1 %to (&periodsCount);
		%let period = %scan(&&periods, &i, '#');

		%CountMissingsForPeriod(&period, &inDataSet, &outDataSet);
	%end;
%mend;

/* </Ex.1> */

/* <Ex.3> */

/*data preparation for outliers calculation */
%macro PrepareDataForOutliersCalc(inDataSet);
	ods exclude all;
	proc means data=&inDataSet.
		N NMiss Mean Min Median Q1 QRange Q3 Max
		STACKODSOUTPUT;

		var _NUMERIC_;
		ods output summary=outLib.dataStats;
	run;
	ods exclude none;

	proc sort data=outLib.dataStats;
		by descending Qrange;
	run;

	data outLib.dataStats;
	    set outLib.dataStats;
	    lowBound=(Q1 - 1.5 * QRange);
	    upBound=(Q3 + 1.5 * QRange);
	run;
%mend;

/*calculates outliers for given variable */
%macro CountOutliersForVariable(var, lower, upper);
	proc sql noprint;
		UPDATE
			outLib.outliers
		SET
			outliers_count = 
			(
			    SELECT COUNT(*)
		        FROM outLib.PREPARED_DATA_FILTERED
		        WHERE &var <= &lower OR &var >= &upper
			)
		WHERE
			Variable = "&var";
	quit;
%mend;

/*calculated outliers for all variables */
%macro CalculateOutliersForAllVariables;
	data outLib.outliers;	
	    set outLib.datastats (keep=Variable N Nmiss lowBound upBound);
		outliers_count = .; 
		call execute(cats('%CountOutliersForVariable(var= ',Variable,', lower= ',lowBound,', upper= ',upBound,');'));
	run;

	proc sort data=outLib.outliers;
		by descending outliers_count;
	run;

	data outLib.outliers;
		set outLib.outliers;
		outliers_participation_all = outliers_count/(N+Nmiss);
		outliers_participation_ = outliers_count/N;
	run;
%mend;

/* drops variables that exceed outliers percent threshold */
%macro FilterOutOutliers(outliersDataSet, inDataSet, outDataSet, threshold);
    data &outDataSet.;
        set &inDataSet.;
    run;

    proc sql noprint;
        SELECT
            Variable
        INTO 
            :variables_to_be_dropped
        SEPARATED BY
            ' '    
        FROM
            &outliersDataSet.
        WHERE
            outliers_participation_all > &threshold;
    quit;

    data &outDataSet.;
        set &outDataSet.;
        drop &&variables_to_be_dropped;
    run;
%mend;

/*invokes outliers calculation chain */
%MACRO InvokeOutliersCalculation;
	%PrepareDataForOutliersCalc(outLib.prepared_data_filtered);
	%CalculateOutliersForAllVariables;
	%FilterOutOutliers(outLib.outliers, outLib.prepared_data_filtered, outLib.data_filtered_outliers, &outliersThreshlod);
%MEND;

/*Calculates outliers for given variable and given period*/
%MACRO CountOutliersForVariableByPeriod(var, lower, upper, per);
	proc sql noprint;
		UPDATE
			outLib.OUTLIERS_BY_PERIOD
		SET
			outliers_count = 
			(
			    SELECT COUNT(*)
		        FROM outLib.PREPARED_DATA
		        WHERE (&var <= &lower OR &var >= &upper) AND period = "&per"
			)
		WHERE
			Variable = "&var" AND period="&per";
	quit;
%MEND;

/* calvulates outliers for given vars and all periods */
%MACRO CountOutliersForVarsAllPeriods(variables_dataset);
	proc sql;
		SELECT 
			Variable
		INTO
			:variables
		SEPARATED BY
			' '	
		FROM
			&variables_dataset;
	run;

	ods output summary=outLib.outliers_by_period;
	proc means data=outLib.prepared_data
		N NMiss Mean Min Median Q1 QRange Q3 Max
		stackods;
		class period;
		var &variables;
	run;
	ods output close;

	data outLib.outliers_by_period;
	    set outLib.outliers_by_period;
	    lowBound=(Q1 - 1.5 * QRange);
	    upBound=(Q3 + 1.5 * QRange);
	run;

	data outLib.outliers_by_period;	
	    set outLib.outliers_by_period (keep=Variable period N Nmiss lowBound upBound);
		outliers_count = .; 
		call execute(cats('%detect_by_period(var= ',Variable,', lower= ',lowBound,', upper= ',upBound,', per= ',period,');'));
	run;
%MEND;

/* </Ex.3> */

/*<Ex.5>*/	
/*calculates vcramer factor for gived variables group name*/
%macro CalculateVCrammerFactor(inDataSet, variablesGroupName, analyzedVariable);
	data outLib.data_filtered_&variablesGroupName;
		set &inDataSet.;
		keep &analyzedVariable. &variablesGroupName.:;
	run;

	proc contents 
		data = outLib.data_filtered_&variablesGroupName 
		out = variables_&variablesGroupName
		noprint;			
	run;

	data variables_&variablesGroupName;
		set variables_&variablesGroupName;
		if NAME NE "&analyzedVariable";
	run;

	proc sql noprint;
		SELECT 
			name
		INTO 
			:variables_list_&variablesGroupName
		SEPARATED BY 
			'#'
		FROM
			variables_&variablesGroupName
	quit;

	%let variablesCount = &sqlobs;

	data vcram_&variablesGroupName;
		length variable $ 80; *30 chars for every variable;
	run;

	%do i = 1 %to (&variablesCount);
		%let variableName = %scan(&&variables_list_&variablesGroupName, &i, '#');

		%put &variableName;

		proc freq data = outLib.data_filtered_&variablesGroupName noprint;
			tables &analyzedVariable. * &variableName / chisq;
			output out = &variableName cramv pchi;
		run;

		data &variableName;
			set &variableName;			
			variable = "&variableName";
			vcram = abs(_cramv_);
		run;

		data vcram_&variablesGroupName;
			set vcram_&variablesGroupName &variableName;
		run;

		proc delete data = &variableName;
		run;
	%end;

	data vcram_&variablesGroupName;
		set vcram_&variablesGroupName;
		keep variable _cramv_ vcram p_pchi;
		where p_pchi <= 0.05;
	run;

	proc sort data = vcram_&variablesGroupName
		out = outLib.vcram_&variablesGroupName;

		by descending vcram;
	run;

	proc delete data = vcram_&variablesGroupName;
	run;

	data outLib.vcram_all_&variablesGroupName;		
		set outLib.vcram_&variablesGroupName;		
	run;

	data outLib.vcram_filtered_&variablesGroupName;		
		set outLib.vcram_&variablesGroupName(obs=5);		
	run;
%mend;

/*invokes vcramer calculation chain for all data groups */
%MACRO CalculateAllVcramerFactors;
	%CalculateVCrammerFactor(outLib.data_filtered_outliers, app, &targetFunction);
	%CalculateVCrammerFactor(outLib.data_filtered_outliers, ags, &targetFunction);
	%CalculateVCrammerFactor(outLib.data_filtered_outliers, agr, &targetFunction);
	%CalculateVCrammerFactor(outLib.data_filtered_outliers, act, &targetFunction);

	data outLib.Vcram_Filtered_All;
	    merge outlib.vcram_filtered_act 		 
			outlib.vcram_filtered_agr	
			outlib.vcram_filtered_ags 
			outlib.vcram_filtered_app;
		by descending vcram;
	run;
%MEND;

/*calulates logistic regression model */
%MACRO CalculateLogisticRegression;
	proc sql;
		SELECT 
			Variable
		INTO
			:variables
		SEPARATED BY
			' '	
		FROM
			outLib.Vcram_Filtered_All;
	run;

	data test;
		set outLib.prepared_data_filtered;
		if missing(&targetFunction.) then &targetFunction. = 0;		
	run;

	ods graphics on;
	proc logistic data=outLib.data_filtered_outliers plots(maxpoints=none)=all;	
		model &targetFunction (Event="1") = &variables / selection=stepwise;
	run;
	ods graphics off;
%MEND;

/* run this first */
	%DataPreparation;


/*<Ex.1>*/
	%FilterVarsMissingThreshold(outLib.prepared_data, outLib.prepared_data_filtered, &missingsThreshold);
/*</Ex.1>*/

/*<Ex.3>*/
	%InvokeOutliersCalculation;
/*</Ex.3>*/

/*<Ex.5>*/
	%CalculateAllVcramerFactors;

	%CalculateLogisticRegression;
/*</Ex.5>*/

/*<for charts>*/

	/*<missings for all periods> */
		/* first run DataPreparation macro */
		%CountMissingsForAllPeriods(outLib.prepared_data, outLib.percentage_missing_period_all);
	/*</missings for all periods> */

	/*<missings for all periods by groups> */
		/* first run DataPreparation macro */
		%CountMissingsForAllPeriods(outLib.data_act, outLib.percentage_missing_period_act);
		%CountMissingsForAllPeriods(outLib.data_ags, outLib.percentage_missing_period_ags);
		%CountMissingsForAllPeriods(outLib.data_agr, outLib.percentage_missing_period_agr);
		%CountMissingsForAllPeriods(outLib.data_app, outLib.percentage_missing_period_app);
	/*</missings for all periods by groups> */

	/*<outliers by period for selected variables*/
		/* first run DataPreparation and CalculateAllVcramerFactors macros*/
		%CountOutliersForVarsAllPeriods(outLib.vcram_filtered_all);
	/*</outliers by period for selected variables*/

/*</for charts>*/