options compress=yes threads;

%let basePath = D:\PROGRAMOWANIE\SAS\PROJEKT_SGH\;

%let inDataPath = "&basePath.DATA\IN";
%let outDataPath = "&basePath.DATA\OUT";
%let macrosPath = "&basePath.ANALYSIS\MACROS";
%let reportsPath = "&basePath.ANALYSIS\REPORTS";

libname inLib &inDataPath access=readonly;
libname outLib &outDataPath;

/*converts char variable to numeric*/
%macro ChangeVariableToNumeric(inDataSet, outDataSet, charVariableName, numericVariableName);
	proc sql noprint;	
		select
			distinct(&charVariableName) AS VALUES
		into
			:char_values
		separated by '#'
		from &inDataSet.;
	quit;

	%let count = &sqlobs;

	%do i = 1 %to (&count);
		%let char_value = %scan(&char_values, &i, '#');

		data &outDataSet.;
			set &outDataSet.;

			if &charVariableName="&char_value" then &numericVariableName = &i;
		run;
	%end;
%mend;

/*invokes data preparation chain */
%macro DataPreparation(inDataSet, outDataSet);
	data &outDataSet.;
		set &inDataSet.;
		length app_num_job_code 3;
		length app_num_marital_status 3;
		length app_num_city 3;
		length app_num_home_status 3;
		length app_num_cars 3;
	run;
 
	%ChangeVariableToNumeric(&inDataSet, &outDataSet, app_char_job_code, app_num_job_code);
	%ChangeVariableToNumeric(&inDataSet, &outDataSet,app_char_marital_status, app_num_marital_status);
	%ChangeVariableToNumeric(&inDataSet, &outDataSet,app_char_city, app_num_city);
	%ChangeVariableToNumeric(&inDataSet, &outDataSet,app_char_home_status, app_num_home_status);
	%ChangeVariableToNumeric(&inDataSet, &outDataSet,app_char_cars, app_num_cars);	
%mend;

/*Creates Train and Valid datasets*/
%MACRO CreateTrainAndValidDatasets;
	proc sql;
		SELECT
			variable
		INTO
			:variables
		SEPARATED BY
			' '
		FROM 
			outLib.vcram_filtered_all;
	run;

	data train;
		set outlib.prepared_data_t;
		keep &variables;
	run;

	data valid;
		set outLib.prepared_data_v;
		keep &variables;
	run;
%MEND;

/*Create own table for all variables in gived dataset*/
%macro CreateTablesForAllColumns(dataSet, suffix);
	proc contents data=&dataSet. noprint 
		out=content(keep=name type where=(type=1));
	run;

	proc sql;
		CREATE TABLE psi_tables_to_process&suffix
		(
			name VARCHAR(50) NOT NULL
		);
	run;

	proc sql noprint;
		SELECT
			name
		INTO
			:variables
		SEPARATED BY
			' '
		FROM 
			content;
	quit;	

	%do i = 1 %to &sqlobs.;	
		%let x = %scan(&variables, &i);
			data &x.&suffix;
				set &dataSet (keep= &x.);
			run;

			proc sql;
				INSERT INTO
					psi_tables_to_process&suffix
				VALUES ("&x.&suffix");
			run;
	%end;
%mend;

/*calculates PSI factor for given variables */
%macro CalculatePsi(tables_to_process_t, tables_to_process_v);
	proc sql;
		CREATE TABLE outLib.psi_all
		(
			variable VARCHAR(50) NOT NULL,
			psi INT
		);
	run;

	%let t_tables_length = 52841;
	%let v_tables_length = 53070;

	%let to_loop_count = %sysfunc(countw(&tables_to_process_t));

	%do i = 1 %to &to_loop_count;
		%let var1 = %scan(&tables_to_process_t,&i);
		%let var2 = %scan(&tables_to_process_v,&i);
		%let table1 = psi_&var1;
		%let table2 = psi_&var2;

		data &table1.;
			set &var1.;
		run;

		data &table2.;
			set &var2.;
		run;

		data &table1.;
			set &table1.;
			BinVar = sum(&var1,(_n_/&t_tables_length));
		run;

		data &table2.;
			set &table2.;
			BinVar = sum(&var1,(_n_/&v_tables_length));
		run;

		proc sort data = &table1.;
			by BinVar;
		run;

		proc sort data = &table2.;
			by BinVar;
		run;

		Proc Format;
			Value DecileF
			Low-0='00'
			0-.1='01'
			.1-.2='02'
			.2-.3='03'
			.3-.4='04'
			.4-.5='05'
			.5-.6='06'
			.6-.7='07'
			.7-.8='08'
			.8-.9='09'
			.9-1='10'
			.='11'
			;
			Value DemiDecileF
			Low-0='00'
			0-.05='01'
			.05-.1='02'
			.1-.15='03'
			.15-.2='04'
			.2-.25='05'
			.25-.3='06'
			.3-.35='07'
			.35-.4='08'
			.4-.45='09'
			.45-.5='10'
			.5-.55='11'
			.55-.6='12'
			.6-.65='13'
			.65-.7='14'
			.7-.75='15'
			.75-.8='16'
			.8-.85='17'
			.85-.9='18'
			.9-.95='19'
			.95-1='20'
			.='21'
			;
			Value ZeroMiss
			0='Zero'
			11='Missing'
			21='Missing'
			;
		run;

		data &table1.;
			Length decile 8.;
			set &table1.;
			Rank=_n_/&t_tables_length;
			Decile=Put(Rank, DecileF.);
		run;

		proc freq data=&table1.;
 			tables decile / out=out1;
		run;

		proc means data=&table1. nway;
	 		class decile;
	 		var BinVar;
 			output out=endpoints max = maxVar;
		run;

		data _NULL_;
			set endpoints end=last;
			file "&basePath.ANALYSIS\TEMP\temp.sas";
			if _N_ = 1 then put " select;";
			put " when (BinVar le " maxVar ") decile = " decile ";" ;
			if last then do ;
				put " otherwise decile = " decile ";" ;
				put "end;";
				call symput('maxbin',decile);
			end;
		run;	
	
		data &table2.;
 			set &table2.;
 			%inc "&basePath.ANALYSIS\TEMP\temp.sas" / source;
			If BinVar = . Then decile = &maxbin;
		run;

		proc freq data=&table2.;
 			tables decile / out=out2;
		run;

		data outLib.p_c_&var1;
	 		merge out1 out2(rename=(percent=percent2));
	 		by decile;
	 		psi = log(percent/percent2)*(percent-percent2);
		run;

		proc sql;
			SELECT
				SUM(psi)
			INTO
				:psi
			FROM
				outLib.p_c_&var1
		run;

		proc sql;		
			INSERT INTO
				outLib.psi_all
			VALUES
			(
				"&var1",
				&psi
			);
		run;

		proc print data=outLib.p_c_&var1 noobs;
	 		var dec: per:;
	 		Format decile ZeroMiss.;
	 		sum psi;
			Title3 "Kalkulacja PSI dla &table1";
		run;
	%end;	
%mend;

/*draws chars for deciles for given variables*/
%macro DrawCharsForAllVariables(tables_to_process);
	%let to_loop_count = %sysfunc(countw(&tables_to_process));

	%do i = 1 %to &to_loop_count;
		%let var = %scan(&tables_to_process,&i);

		goptions reset=all cback=white border htext=10pt htitle=12pt ypixels=200 xpixels=300; 
		axis1 label=("[%]") order=(9.6 to 10.4 by 0.2);
		axis2 offset=(2,2)pct label=("decyle");
		legend1 label=("zbiory danych") value=("train" "valid");
		ods graphics on;
		symbol1 interpol=sm value=dot color=vibg;                                                                                           
		symbol2 interpol=sm value=dot color=depk;
		title "&var"; 
		proc gplot data=outLib.p_c_&var;
			plot (percent percent2)*decile / overlay 
											vaxis=axis1 
											haxis=axis2
											legend=legend1;
		run;
		quit;
		ods graphics off;
	%end;
%mend;

/*invokes psi calculation process*/
/*WARNING! you need outLib.vcram_filtered_all dataset to run this process*/
/*you can generate it using main.sas file*/
/*you can replace this with any data set that contains list of variables*/
%MACRO InvokePsiCalculationProcess;
	%DataPreparation(inLib.abt_sam_beh_train, outLib.prepared_data_t);
	%DataPreparation(inLib.abt_sam_beh_valid, outLib.prepared_data_v);

	%CreateTrainAndValidDatasets;

	%CreateTablesForAllColumns(train);
	%CreateTablesForAllColumns(valid, _v);

	proc sql;
		SELECT
			name
		INTO
			:tables_to_process
		SEPARATED BY
			' '
		FROM 
			psi_tables_to_process;
	run;

	proc sql;
		SELECT
			name
		INTO
			:tables_to_process_v
		SEPARATED BY
			' '
		FROM 
			psi_tables_to_process_v;
	run;

	%CalculatePsi(&tables_to_process, &tables_to_process_v);

	%DrawCharsForAllVariables(&tables_to_process);
%MEND;