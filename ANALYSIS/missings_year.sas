﻿options compress=yes threads;
ods graphics on;
ods trace on;

%let basePath = C:\Users\Admin\Desktop\projekt_sas\;

%let inDataPath = "&basePath.DATA\IN";
%let outDataPath = "&basePath.DATA\OUT";

%let inData = inData.abt_sam_beh_train;

libname inLib &inDataPath access=readonly;
libname outLib &outDataPath;

/*Calculates Missings for givem variables group*/
%macro CountMissingsForVar(var);
	data data_&var;
		set &inData.;
		keep &var:;
	run;

	ods output summary=summary_stats;
	proc means data=data_&var stackods n nmiss nway;		
	run;	

	data stats_t_percent_&var;
		set summary_stats;
		missing_data_percentage = (nmiss/sum(n, nmiss))*100;
	run;

	proc summary data=stats_t_percent_&var nway;
		var missing_data_percentage;	
		output out=avg (drop=_:) mean=;
	run;

	data percentage_missing_&var;
		period = "&var";
		set avg;	
	run;

	proc delete data=data_&var;
	run;

	proc delete data=summary_stats;
	run;

	proc delete data=stats_t_percent_&var;
	run;

	proc delete data=avg;
	run;

	ods output close;
%mend;

%CountMissingsForVar(ags);
%CountMissingsForVar(agr);
%CountMissingsForVar(app);
%CountMissingsForVar(act);
%CountMissingsForVar(default);

data outLib.percentage_missing_variables;

    merge 
		percentage_missing_ags		 
		percentage_missing_agr		
		percentage_missing_app		
		percentage_missing_act	
		percentage_missing_default;
	by descending missing_data_percentage;
run;

/*calculates missing percent for given period*/
%macro CountMissingsForPeriod(period);
	data data_&period;
		set &inData.;
		where period like "&period%";
	run;

	ods output summary=summary_stats;
	proc means data=data_&period stackods n nmiss nway;		
	run;	

	data stats_t_percent_&period;
		set summary_stats;
		missing_data_percentage = (nmiss/sum(n, nmiss))*100;
	run;

	proc summary data=stats_t_percent_&period nway;
		var missing_data_percentage;	
		output out=avg (drop=_:) mean=;
	run;

	data percentage_missing_&period;
		period = "&period";
		set avg;	
	run;

	proc delete data=data_&period;
	run;

	proc delete data=summary_stats;
	run;

	proc delete data=stats_t_percent_&period;
	run;

	proc delete data=avg;
	run;

	ods output close;
%mend;

%CountMissingsForPeriod(2004);
%CountMissingsForPeriod(2005);
%CountMissingsForPeriod(2006);
%CountMissingsForPeriod(2007);
%CountMissingsForPeriod(2008);
%CountMissingsForPeriod(2009);
%CountMissingsForPeriod(2010);
%CountMissingsForPeriod(2011);
%CountMissingsForPeriod(2012);
%CountMissingsForPeriod(2013);
%CountMissingsForPeriod(2014);
%CountMissingsForPeriod(2015);
%CountMissingsForPeriod(2016);
%CountMissingsForPeriod(2017);
%CountMissingsForPeriod(2018);

data outLib.percentage_missing_year;

    merge 
		percentage_missing_2004		 
		percentage_missing_2005		
		percentage_missing_2006	
		percentage_missing_2007	
		percentage_missing_2008
		percentage_missing_2009		 
		percentage_missing_2010		
		percentage_missing_2011	
		percentage_missing_2012	
		percentage_missing_2013
		percentage_missing_2014		 
		percentage_missing_2015		
		percentage_missing_2016	
		percentage_missing_2017	
		percentage_missing_2018;
	by period;
run;

%macro CountMissingsForAllPeriods;
	proc sql;
		CREATE TABLE outLib.percentage_missing_periods 
		(
			period VARCHAR NOT NULL,
			missing_data_percentage INT NOT NULL
		);
	run;
	
	proc sql;
		SELECT DISTINCT
			period
		INTO
			:periods
		SEPARATED BY
			'#'
		FROM
			&inData.;
	run;

	%let periodsCount = &sqlObs;

	%do i = 1 %to (&periodsCount);
		%let period = %scan(&&periods, &i, '#');

		%CountMissingsForPeriod(&period);
	%end;
%mend;

%CountMissingsForAllPeriods;


/*calculations for default_cus12 missings*/
data missing_default_cus12;
	set stats_t_percent;
	where variable = "default_cus12";
	drop n nmiss;
run;


%macro CountMissings12ForPeriod(period);
	data data_&period;
		set &inData.;
		where period like "&period%";
		keep default_cus12;
	run;

	ods output summary=summary_stats;
	proc means data=data_&period stackods n nmiss nway;		
	run;	

	data stats_t_percent_&period;
		set summary_stats;
		missing_data_percentage = (nmiss/sum(n, nmiss))*100;
	run;

	proc summary data=stats_t_percent_&period nway;
		var missing_data_percentage;	
		output out=avg (drop=_:) mean=;
	run;

	data percentage_missing12_&period;
		period = "&period";
		set avg;	
	run;

	proc delete data=data_&period;
	run;

	proc delete data=summary_stats;
	run;

	proc delete data=stats_t_percent_&period;
	run;

	proc delete data=avg;
	run;

	ods output close;
%mend;

%CountMissings12ForPeriod(2004);
%CountMissings12ForPeriod(2005);
%CountMissings12ForPeriod(2006);
%CountMissings12ForPeriod(2007);
%CountMissings12ForPeriod(2008);
%CountMissings12ForPeriod(2009);
%CountMissings12ForPeriod(2010);
%CountMissings12ForPeriod(2011);
%CountMissings12ForPeriod(2012);
%CountMissings12ForPeriod(2013);
%CountMissings12ForPeriod(2014);
%CountMissings12ForPeriod(2015);
%CountMissings12ForPeriod(2016);
%CountMissings12ForPeriod(2017);
%CountMissings12ForPeriod(2018);

data outLib.percentage_missing12_year;

    merge 
		percentage_missing12_2004		 
		percentage_missing12_2005		
		percentage_missing12_2006	
		percentage_missing12_2007	
		percentage_missing12_2008
		percentage_missing12_2009		 
		percentage_missing12_2010		
		percentage_missing12_2011	
		percentage_missing12_2012	
		percentage_missing12_2013
		percentage_missing12_2014		 
		percentage_missing12_2015		
		percentage_missing12_2016	
		percentage_missing12_2017	
		percentage_missing12_2018;
	by period;
run;