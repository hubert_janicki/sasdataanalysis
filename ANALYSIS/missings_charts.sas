/*WARNING! To create charts you need to run process chain in main.sas*/

options compress=yes threads;

%let basePath = D:\PROGRAMOWANIE\SAS\PROJEKT_SGH\;

/*here set directory for dataset with train or valid datasets*/
%let outDataPath = "&basePath.DATA\OUT";

libname outLib &outDataPath;

data act_renamed;
	set outLib.percentage_missing_period_act;
	rename missing_data_percentage = missing_act;
run;

proc sort data=act_renamed;
   BY period;
run;

data agr_renamed;
	set outLib.percentage_missing_period_agr;
	rename missing_data_percentage = missing_agr;
run;

proc sort data=agr_renamed;
   BY period;
run;

data ags_renamed;
	set outLib.percentage_missing_period_ags;
	rename missing_data_percentage = missing_ags;
run;

proc sort data=ags_renamed;
   BY period;
run;

data app_renamed;
	set outLib.percentage_missing_period_app;
	rename missing_data_percentage = missing_app;
run;

proc sort data=app_renamed;
   BY period;
run;

data all_renamed;
	set outLib.percentage_missing_period_all;
	rename missing_data_percentage = missing_all;
run;

proc sort data=all_renamed;
   BY period;
run;

DATA merged;
	MERGE act_renamed
		ags_renamed
		agr_renamed
		app_renamed
		all_renamed;		
	BY period;
RUN;


title1 "�redni procent brak�w danych dla grup zmiennych w poszczeg�lnym miesi�cach - train";

/* Define symbol characteristics */                                                                                                     
symbol1 interpol=join interpol=join  color=vibg height=2;                                                                         
symbol2 interpol=join interpol=join color=depk height=2;                                                                         
symbol3 interpol=join interpol=join  color=mob  height=2;
symbol4 interpol=join interpol=join  color=STYBR  height=2;
symbol5 interpol=join interpol=join  color=BRGR   height=2; 
legend1 label=none frame;  
axis1 label=("period") minor=none offset=(1,1) );                                                                                     
axis2 label=(angle=90 "% missing values")                                                                                                     
      order=(0 to 100 by 10) minor=(n=1);  
proc gplot data= merged;                                                                                                               
   plot (missing_act missing_agr missing_ags missing_all missing_app) * period / overlay legend=legend1                                                                           
                   haxis=axis1 vaxis=axis2;                                                                       
   format YearQuarter year4.;                                                                                                           
run;                                                                                                                                    
quit;  


